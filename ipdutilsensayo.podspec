Pod::Spec.new do |s|
  s.name         = "ipdutilsensayo"
  s.version      = "0.0.4"
  s.summary      = "A short description of kapptaLibrary."
  s.description  = <<-DESC
  descripcion
                   DESC

  s.homepage     = "https://bitbucket.org/iphonedroid/ipdutilsensayo.git"
  s.license      = "MIT"
  s.author             = { "iphoneDroid" => "info@iphonedroid.com" }
  s.source       = { :git => "https://bitbucket.org/iphonedroid/ipdutilsensayo.git", :tag => "#{s.version}" }
  s.source_files  = 'ipdutilsensayo/ipdutilsensayo/*.swift' 
  s.social_media_url = 'https://bitbucket.org/iphonedroid/ipdutilsensayo.git'  
  s.documentation_url = 'https://bitbucket.org/iphonedroid/ipdutilsensayo.git'
  s.ios.deployment_target = '10.0'
  s.frameworks = 'CFNetwork'
end

